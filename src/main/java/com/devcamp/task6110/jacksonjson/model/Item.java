package com.devcamp.task6110.jacksonjson.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")

public class Item {
    public int id;
    public String itemName;

    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    //@JsonManagedReference
    public User user;

    public Item(int id, String itemName, User user) {
        this.id = id;
        this.itemName = itemName;
        this.user = user;
    }
}
