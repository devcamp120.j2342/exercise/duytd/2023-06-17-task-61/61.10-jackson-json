package com.devcamp.task6110.jacksonjson.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task6110.jacksonjson.model.Item;
import com.devcamp.task6110.jacksonjson.model.User;

@CrossOrigin
@RestController
public class UserController {

    @GetMapping("/users")
    public User getUser() {
        User user = new User(1, "Jhon");
        Item item = new Item(1, "book", user);

        user.addItem(item);
        return user;
    }
    
    @GetMapping("/items")
    public Item getItem() {
        User user = new User(1, "Jhon");
        Item item = new Item(1, "book", user);

        user.addItem(item);
        return item;
    }
}
