package com.devcamp.task6110.jacksonjson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JacksonJsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(JacksonJsonApplication.class, args);
	}

}
